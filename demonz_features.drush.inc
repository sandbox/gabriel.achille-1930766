<?php

/**
 * @file
 * Features module drush integration.
 */

/**
 * Implements hook_drush_command().
 *
 * @return
 *   An associative array describing your command(s).
 *
 * @see drush_parse_command()
 */
function demonz_features_drush_command() {
  $items = array();

  $items['features-update-all-ordered'] = array(
    'description' => "Update all feature modules on your site using the order provide ...TODO...",
    'arguments' => array(
      'feature_exclude' => 'A space-delimited list of features to exclude from being updated.',
      ),
    'drupal dependencies' => array('features'),
    'aliases' => array('fu-all-o', 'fuao'),
    );
  $items['features-update-scope'] = array(
    'description' => "Update the scope of a feature module on your site.",
    'arguments' => array(
      'feature' => 'A space delimited list of features.',
      ),
    'drupal dependencies' => array('features'),
    'aliases' => array('fus'),
    );
  $items['features-update-scope-all'] = array(
    'description' => "Update the scope of all feature modules on your site.",
    'arguments' => array(
      'feature_exclude' => 'A space-delimited list of features to exclude from being updated.',
      ),
    'options' => array(
      'update-content' => array(
        'description' => 'run a fu just after scope update for each module.',
        ),
      ),
    'drupal dependencies' => array('features'),
    'aliases' => array('fus-all', 'fusa'),
    );
  return $items;
}

/**
 * Implements hook_drush_help().
 */
function demonz_features_drush_help($section) {
  switch ($section) {
    case 'drush:features-update-all-ordered':
      return dt("Update all feature modules on your site using the order provide ...TODO.... The order have to be compliant with existing dependencies");
    case 'drush:features-update-scope':
      return dt("Update the SCOPE of a feature module on your site. The component types have to be defined in the admin settings (...)");
    case 'drush:features-update-scope-all':
      return dt("Update the scope of all feature modules on your site. The component types have to be defined in the admin settings (...)");
  }
}


/**
 * Update all enabled features. Optionally pass in a list of features to
 * exclude from being updated.
 */
function drush_demonz_features_features_update_all_ordered() {

  $features_to_update = array();
  $features_to_exclude = func_get_args();
  $ordered_modules_list = explode("\n", str_replace("\r", "", variable_get('features_update_modules_ordered_list', '')));

  $all_features = features_get_features();
  foreach ($ordered_modules_list as $module_name) {
    if (array_key_exists($module_name, $all_features)
      && $all_features[$module_name]->status
      && !in_array($module_name, $features_to_exclude)) {
      $features_to_update[] = $module_name;
    }
  }

  drush_print(dt('The following modules will be updated: !modules', array('!modules' => implode(', ', $features_to_update))));
  if (drush_confirm(dt('Do you really want to continue?'))) {
    foreach ($features_to_update as $module_name) {
      drush_invoke_process(drush_sitealias_get_record('@self'), 'features-update', array($module_name));
    }
  }
  else {
    drush_die('Aborting.');
  }
}


/**
 * Update the SCOPE of an existing feature module
 */
function drush_demonz_features_features_update_scope() {

  if ($args = func_get_args()) {
    $components = _drush_features_component_list();
    foreach ($args as $module) {
      if ( module_exists($module) && ($feature = features_load_feature($module, TRUE)) ) {
        //1- get the information
        // 1-1 read features_update_component_type
        if (isset($feature->info['features_update_component_type'])) {
          // 1-2 get all non-already exported components
          $filtered_components = array();
          $components_map = features_get_component_map();
          foreach ($feature->info['features_update_component_type'] as $comp_type) {
            foreach ($components[$comp_type] as $comp_name => $comp_content) {
              if (!isset($components_map[$comp_type][$comp_name])) {
                $filtered_components[$comp_type][$comp_name] = $comp_name;
              }
            }
          }
          // 1-3 exclude exclusion
          foreach($filtered_components as $component_type => $component_list) {
            if (isset($feature->info['features_update_exclude_component'][$component_type])) {
              $components_to_add = $component_list;
              foreach ($feature->info['features_update_exclude_component'][$component_type] as $pattern) {
                // Rewrite * to %. Let users use both as wildcard.
                $pattern = strtr($pattern, array('*' => '%'));

                $preg_pattern = strtr(preg_quote($pattern, '/'), array('%' => '.*'));
                /*
                 * If it isn't a pattern, but a simple string, we don't anchor the
                 * pattern, this allows for abbreviating. Else, we do, as this seems more
                 * natural for patterns.
                 */
                if (strpos($pattern, '%') !== FALSE) {
                  $preg_pattern = '^' . $preg_pattern . '$';
                }
                $matches = array();
                // Find the sources.
                $all_sources = array_keys($components_to_add);
                $matches = preg_grep('/' . $preg_pattern . '/', $all_sources);
                if (sizeof($matches) > 0) {
                  // remove matches from components_to_add:
                  $components_to_add = array_diff_key($components_to_add, array_flip($matches));
                }
              }
              //$components_to_add = array_diff_key($component_list, array_flip($feature->info['features_update_exclude_component'][$component_type]));
            }
            else {
              $components_to_add = array_keys($component_list);
            }
          }
          if (!empty($components_to_add)) {
            //2- call features export with the arguments just retrieved
            // 2-1 build the arg list:
            // first argument is the module name:
            $arguments = array($module);
            // next ones are the components to add:
            foreach($components_to_add as $comp) {
              $arguments[] = $component_type.':'.$comp;
            }
            //print("component to add:".PHP_EOL);
            //print_r($arguments);
            drush_print(dt('@comp_count component(s) of @component_type type will be added to @module_name feature: @components_to_add.', array('@comp_count' => count($components_to_add), '@component_type' => $component_type, '@module_name' => $module, '@components_to_add' => implode(', ', $components_to_add)) ));
            call_user_func_array("drush_features_export", $arguments);
          }
          else {
            drush_log(dt('There are no new component to add in @module_name module.', array('@module_name' => $module)));
          }

        }
        else {
          return drush_set_error('', dt('The feature @module_name is not scope-updatable.', array('@module_name' => $module)));
        }

      }
      else if ($feature) {
        _features_drush_set_error($module, dt('FEATURES_FEATURE_NOT_ENABLED'));
      }
      else {
        _features_drush_set_error($module);
      }
    }
  }
  else {
    // By default just show contexts that are available.
    $rows = array(array(dt('Available features')));
    foreach (features_get_features(NULL, TRUE) as $name => $info) {
      if (isset($info->info['features_update_component_type'])) {
        $rows[] = array($name);
      }
    }
    drush_print(dt('Please specify a feature module that does support features_update'));
    drush_print_table($rows, TRUE);
  }
}

/**
 * Update the SCOPE of all enabled features. Optionally pass in a list of features to
 * exclude from being updated.
 */
function drush_demonz_features_features_update_scope_all() {
  $features_to_update = array();
  $features_to_exclude = func_get_args();

  $updatecontent = FALSE;
  if (drush_get_option(array('update-content', 'u'), NULL)) {
    $updatecontent = TRUE;
  }

  $ordered_modules_list = explode("\n", str_replace("\r", "", variable_get('features_update_modules_ordered_list', '')));
  if (!empty($ordered_modules_list)) {
    // specific list and order
    $all_features = features_get_features();
    foreach ($ordered_modules_list as $module_name) {
      if (array_key_exists($module_name, $all_features)
        && $all_features[$module_name]->status
        && !in_array($module_name, $features_to_exclude)
        && isset($all_features[$module_name]->info['features_update_component_type'])) {
          $features_to_update[] = $module_name;
        }
    }
  }
  else {
      // default list and order:
    foreach (features_get_features() as $module) {
      if ($module->status && !in_array($module->name, $features_to_exclude) && isset($module->info['features_update_component_type'])) {
        $features_to_update[] = $module->name;
      }
    }
  }


  drush_print(dt('The following modules will be scope-updated: !modules', array('!modules' => implode(', ', $features_to_update))));
  if (drush_confirm(dt('Do you really want to continue?'))) {
    foreach ($features_to_update as $module_name) {
      drush_print($module_name.':BEGIN');
      drush_invoke_process(drush_sitealias_get_record('@self'), 'features-update-scope', array($module_name));
      if ($updatecontent) {
        drush_print($module_name.':MIDDLE');
        drush_invoke_process(drush_sitealias_get_record('@self'), 'features-update', array($module_name));
      }
      drush_print($module_name.':END');
    }
  }
  else {
    drush_die('Aborting.');
  }
}
